/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author uvic
 */
public class SchemaLoaderImplTest {
    private static final FileUtils FILE_UTILS = new FileUtilsImpl();
    
    public SchemaLoaderImplTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void GivenFromNull_whenToUtf8_ThenInvalidSourceExertion() {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FILE_UTILS.toUtf8(ValidCharset.ASCII, null, "src/test/resources/utf8.txt");
        });
    }

    @Test
    public void GivenToNull_whenToUtf8_ThenToCreate() {
        Assertions.assertThrows(InvalidSourceException.class, () -> {
            FILE_UTILS.toUtf8(ValidCharset.ASCII, "src/test/resources/utf8.txt", null);
        });

    }
}
