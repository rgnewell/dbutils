/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.DbUtilsException;
import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;

import java.io.File;
import java.nio.file.Path;

/**
 *
 * @author uvic
 */
public class SchemaLoaderImpl implements SchemaLoader {

    @Override
    public void load(String path) throws InvalidSourceException, DbUtilsException {
        File f = new File(path);

        if (!f.exists() || f.isDirectory()){
            throw new InvalidSourceException(path +  "Dosn't exists or is a directory");
        }

        if (f == null || f.listFiles().length == 0 || f.getAbsolutePath().contains(".sql")){ // do it in different parts
            throw new DbUtilsException(path +  "isn't .sql, its empty or its null");
        }


    }
    
}
