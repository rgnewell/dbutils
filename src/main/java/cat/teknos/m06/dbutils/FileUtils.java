package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;

import java.io.IOException;

public interface FileUtils {
     void toUtf8(ValidCharset charset, String from,String to) throws InvalidSourceException, IOException;

}
