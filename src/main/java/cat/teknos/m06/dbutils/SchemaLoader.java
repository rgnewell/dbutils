/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.DbUtilsException;
import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;

/**
 *
 * @author uvic
 */
public interface SchemaLoader {
    void load(String path) throws InvalidSourceException, DbUtilsException;

}
