package cat.teknos.m06.dbutils;

public enum ValidCharset {
    ASCII,
    ISO8859,
    UTF8,
}
