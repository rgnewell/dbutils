package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.DbUtilsException;
import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;

import java.io.*;
import java.nio.charset.Charset;

//obrir filewriter amb utf-8 i escribir-lo al to

//fer test amb la llibreria que vem fer avans
//mirar que els dos tinguin el amteix contingut
public class FileUtilsImpl implements FileUtils {
    private static final Charset UTF8 = Charset.forName("UTF8");
    
    @Override
public void toUtf8(ValidCharset validcharset, String from, String to) throws InvalidSourceException, DbUtilsException {
        
        if(from == null || to == null){
            
        }
        
        var fileFrom = new File(from);
        var fileTo = new File(to);
        var charset = getCharset(validcharset);

        // TODO: tests
        try(
                var reader = new BufferedReader(new FileReader(fileFrom, charset));
                var writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileTo),"UTF8"))
                
                ){
           String line = null;
           while ((line = reader.readLine())!= null){
               System.out.println(line);
               writer.write(line);
           }
            reader.readLine();
            

        }catch(FileNotFoundException e) {
            System.out.println(e);
        } catch(IOException e) {

        } finally{
             //reader.close;
            //writer.close;
        }
           

    }

    private static Charset getCharset(ValidCharset validCharset){
        Charset charset;
        switch (validCharset){
            case ASCII:
                charset = Charset.forName("US-ASCII");
                break;
            case ISO8859:
                charset = Charset.forName("ISO-8859-1");
                break;
            case UTF8:
                charset = Charset.forName("UTF8");
                break;
            default:
                charset = Charset.forName("UTF16");
                break;

        }
        return charset;
    }

    public boolean isValidFile(){
        return false;
    }
}
