package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exceptions.InvalidSourceException;

import java.io.IOException;

public class Program {
    public static void main(String[] args ) throws InvalidSourceException {
        //ASCII
        new FileUtilsImpl().toUtf8(ValidCharset.ASCII,"src/main/resources/ascii.txt", "src/main/resources/to.txt");

        //ISO8859
        //new FileUtilsImpl().toUtf8(ValidCharset.ISO8859,"src/main/resources/iso.txt", null);

        //UTF-8
        //new FileUtilsImpl().toUtf8(ValidCharset.UTF8,"src/main/resources/utf8.txt", null);

        //UTF-16
        //new FileUtilsImpl().toUtf8(ValidCharset.UTF16,"src/main/resources/utf16.txt", null);
    }
}
